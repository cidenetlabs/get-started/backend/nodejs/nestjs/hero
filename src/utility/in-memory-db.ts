import { Injectable } from '@nestjs/common';
import { Hero } from '../hero/hero';

@Injectable()
export class HeroRepository {
  private heroes: Hero[] = [
      { id: 11, name: 'Dr Nice' },
      { id: 12, name: 'Narco' },
      { id: 13, name: 'Bombasto' },
      { id: 14, name: 'Celeritas' },
      { id: 15, name: 'Magneta' },
      { id: 16, name: 'RubberMan' },
      { id: 17, name: 'Dynama' },
      { id: 18, name: 'Dr IQ' },
      { id: 19, name: 'Magma' },
      { id: 20, name: 'Tornado' },
    ];

  findAll(): Hero[] {
    return this.heroes;
  }

  findById(id: number): Hero {
    const hero = this.heroes.find(h => h.id === id);
    return hero;
  }

  update(hero: Hero): Hero {
    let oldHero = this.findById(hero.id);
    if (oldHero) {
      oldHero = hero;
      return oldHero;
    } else {
      return null;
    }
  }

  save(hero: Hero): Hero {
    hero.id = this.heroes.length + 11;
    this.heroes.push(hero);
    return hero;
  }

  delete(hero: Hero): boolean {
    this.heroes.splice(this.heroes.indexOf(hero), 1);
    return true;
  }
}
