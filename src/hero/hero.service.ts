import { Injectable } from '@nestjs/common';
import { Hero } from './hero';
import { HeroRepository } from '../utility/in-memory-db';

@Injectable()
export class HeroService {
  constructor(private readonly heroRepository: HeroRepository) {}

  getAllHeroes(): Hero[] {
    return this.heroRepository.findAll();
  }

  getHero(id: number): Hero {
    return this.heroRepository.findById(id);
  }

  saveHero(hero: Hero): Hero {
    if (null !== hero.id) {
      return this.heroRepository.update(hero);
    }
    return this.heroRepository.save(hero);
  }

  deleteHero(id: number): boolean {
    const hero = this.getHero(id);
    return this.heroRepository.delete(hero);
  }

}
