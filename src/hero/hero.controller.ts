import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { HeroService } from './hero.service';
import { Hero } from './hero';
import { IntPipe } from '../utility/int.pipe';

@Controller('heroes')
export class HeroController {
  constructor(private readonly heroService: HeroService) {}

  @Get()
  getAllHeroes(): Hero[] {
    return this.heroService.getAllHeroes();
  }

  @Get(':id')
  getHero(@Param('id', new IntPipe()) id: number): Hero {
    return this.heroService.getHero(id);
  }

  @Post()
  saveHero(@Body() hero: Hero): Hero {
    return this.heroService.saveHero(hero);
  }

  @Put(':id')
  updateHero(@Param('id', new IntPipe()) id: number, @Body() hero: Hero): Hero {
    if (id === hero.id) {
      return this.heroService.saveHero(hero);
    } else {
      return null;
    }
  }

  @Delete(':id')
  deleteHero(@Param('id', new IntPipe()) id: number): boolean {
    return this.heroService.deleteHero(id);
  }
}
