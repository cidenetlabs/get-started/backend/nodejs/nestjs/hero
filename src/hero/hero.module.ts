import { Module } from '@nestjs/common';
import { HeroController } from './hero.controller';
import { HeroService } from './hero.service';
import { HeroRepository } from '../utility/in-memory-db';

@Module({
  imports: [],
  controllers: [HeroController],
  providers: [HeroService, HeroRepository],
})
export class HeroModule {}
