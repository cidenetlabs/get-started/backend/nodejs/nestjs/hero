# Description

Hero Service based on NodeJS with [NestJS](https://github.com/nestjs/nest) Framework
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# integration tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
